﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GamePlayController : MonoBehaviour
{
    public static GamePlayController instance;

    public float moveSpeed, distanceFactor = 1f;
    private float distanceMove;
    private bool isGameJustStarted;

    public GameObject obstaclesObj;
    public GameObject[] obstaclesList;
    [HideInInspector]
    public bool isObstaclesActive;
    private string CoroutineName = "SpawnObstaclesRoutine";

    private Text txtScore;
    private Text txtStarScore;

    private int starScoreCount, scoreCount;

    public GameObject pausePanel;
    public Animator pauseAnimator;

    public GameObject gameOverPanel;
    public Animator gameOverAnimator;

    public Text txtFinalScore, txtBesScore, txtFinalStarScore;

    private void Awake()
    {
        MakeInstance();

        txtScore = GameObject.Find("txtScoreUI").GetComponent<Text>();
        txtStarScore = GameObject.Find("txtStarScoreUI").GetComponent<Text>();
    }

    void Start()
    {
        isGameJustStarted = true;
        GetObstacles();
        StartCoroutine(CoroutineName);
    }

    void Update()
    {
        MoveCamera();
    }

    void MakeInstance()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            Destroy(gameObject);
        }
    }

    void MoveCamera()
    {
        if (isGameJustStarted)
        {
            if (!PlayerController.instance.isPlayerDied)
            {
                if (moveSpeed < 12.0f)
                {
                    moveSpeed += Time.deltaTime * 5.0f;
                }
                else
                {
                    moveSpeed = 12f;
                    isGameJustStarted = false;
                }
            }
        }

        if (!PlayerController.instance.isPlayerDied)
        {
            Camera.main.transform.position += new Vector3(moveSpeed * Time.deltaTime, 0f, 0f);
            UpdateDistance();
        }
    }

    void UpdateDistance()
    {
        distanceMove += Time.deltaTime * distanceFactor;
        float round = Mathf.Round(distanceMove);

        scoreCount = (int)round;
        txtScore.text = scoreCount.ToString();

        if (round >= 30.0f && round < 60.0f)
        {
            moveSpeed = 14f;
        }
        else if (round >= 60f)
        {
            moveSpeed = 16f;
        }
    }

    void GetObstacles()
    {
        obstaclesList = new GameObject[obstaclesObj.transform.childCount];

        for (int i = 0; i < obstaclesList.Length; i++)
        {
            obstaclesList[i] = obstaclesObj.GetComponentsInChildren<ObstacleHolder>(true)[i].gameObject;
        }
    }

    IEnumerator SpawnObstaclesRoutine()
    {
        while (true)
        {
            if (!PlayerController.instance.isPlayerDied)
            {
                if (!isObstaclesActive)
                {
                    if (Random.value <= 0.85f)
                    {
                        int randomIndex = 0;

                        do
                        {
                            randomIndex = Random.Range(0, obstaclesList.Length);
                        } while (obstaclesList[randomIndex].activeInHierarchy);

                        obstaclesList[randomIndex].SetActive(true);
                        isObstaclesActive = true;
                    }
                }
            }
            yield return new WaitForSeconds(0.6f);
        }
    }

    public void UpdateStarScore()
    {
        starScoreCount++;
        txtStarScore.text = starScoreCount.ToString();
    }

    public void PauseGame()
    {
        Time.timeScale = 0f;
        pausePanel.SetActive(true);
        pauseAnimator.Play("SlideIn");
    }

    public  void ResumeGame()
    {
        pauseAnimator.Play("SlideOut");
    }

    public void RestartGame()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);//pega a cena em que o player esta e recarrega
    }

    public void HomeButton()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("MainMenu");//pega a cena em que o player esta e recarrega
    }

    public void GameOver()
    {
        Time.timeScale = 0;
        gameOverPanel.SetActive(true);
        gameOverAnimator.Play("SlideIn");

        txtFinalScore.text = scoreCount.ToString();
        txtFinalStarScore.text = starScoreCount.ToString();

        if (GameManager.instance.scoreCount < scoreCount)
        {
            GameManager.instance.scoreCount = scoreCount;//novo score
        }

        txtBesScore.text = "BEST <color='yellow'>" + GameManager.instance.scoreCount.ToString() + "</color>";

        GameManager.instance.starScore += starScoreCount;
        GameManager.instance.SaveGameData();
    }

}
