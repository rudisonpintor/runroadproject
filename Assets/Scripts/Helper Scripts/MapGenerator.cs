﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    public static MapGenerator instance;

    public GameObject roadPrefab,
        grassPrefab,
        groundPrefab_1,
        groundPrefab_2,
        groundPrefab_3,
        groundPrefab_4,
        grassBottomPrefab,
        landPrefab_1,
        landPrefab_2,
        landPrefab_3,
        landPrefab_4,
        landPrefab_5,
        bigGrassPrefab,
        bigGrassBottomPrefab,
        treePrefab_1,
        treePrefab_2,
        treePrefab_3,
        bigTreePrefab;

    public GameObject roadHolder,
        topNearSideWalkHolder,
        topFarSideWalkHolder,
        bottomNearSideWalkHolder,
        bottomFarSideWalkHolder;

    public int startRoadTile, //Inicializa numero de 'Road' tiles
        startGrassTile,//Inicializa numero de 'grass' tiles
        startGround3Tile,//Inicializa numero de 'ground3' tiles
        startLandTile;//Inicializa numero de 'land' tiles

    public List<GameObject>
        roadTilesList,
        topNearGrassTilesList,
        topFarGrassTilesList,
        bottomNearGrassTilesList,
        bottomFarLandF1TilesList,
        bottomFarLandF2TilesList,
        bottomFarLandF3TilesList,
        bottomFarLandF4TilesList,
        bottomFarLandF5TilesList;

    //posicao para ground1 no topo 0 para startGround3tile    //near = perto; far = longe
    public int[] posForTopGround1;
    //posicao para ground2 no topo 0 para startGround3tile
    public int[] posForTopGround2;
    //posicao para ground4 no topo 0 para startGround3tile
    public int[] posForTopGround4;

    //posicao para bigGrass com arvores no topo perto da grama de 0 para startGround3tile
    public int[] posForTopBigGrass;

    //posicão para arvore1 no topo proximo a grama de 0 para startGrassTile
    public int[] posForTopTree1;
    //posicão para arvore2 no topo proximo a grama de 0 para startGrassTile
    public int[] posForTopTree2;
    //posicão para arvore3 no topo proximo a grama de 0 para startGrassTile;
    public int[] posForTopTree3;

    //posicão do tile road na estrada de 0 para startRoadTile;
    public int posForRoadTile1;
    //posicão do tile road na estrada de 0 para startRoadTile;
    public int posForRoadTile2;
    //posicão do tile road na estrada de 0 para startRoadTile;
    public int posForRoadTile3;

    //posicao para bigGrass com arvore em baixo da proxima grama de 0 para startGrassTile
    public int[] posForBottomBigGrass;

    //posicao para tree1 em baixo proximo a grama de 0 para startGrassTile
    public int[] posForBottomTree1;
    //posicao para tree2 em baixo proximo a grama de 0 para startGrassTile
    public int[] posForBottomTree2;
    //posicao para tree3 em baixo proximo a grama de 0 para startGrassTile
    public int[] posForBottomTree3;

    [HideInInspector]
    public Vector3
        lastPosOfRoadTile,
        lastPosOfTopNearGrass,
        lastPosOfTopFarGrass,
        lastPosOfBottomNearGrass,
        lastPosOfBottomFarLandF1,
        lastPosOfBottomFarLandF2,
        lastPosOfBottomFarLandF3,
        lastPosOfBottomFarLandF4,
        lastPosOfBottomFarLandF5;

    [HideInInspector]
    public int
        lastOrderOfRoad,
        lastOrderOfTopNearGrass,
        lastOrderOfTopFarGrass,
        lastOrderOfBottomNearGrass,
        lastOrderOfBottomFarLandF1,
        lastOrderOfBottomFarLandF2,
        lastOrderOfBottomFarLandF3,
        lastOrderOfBottomFarLandF4,
        lastOrderOfBottomFarLandF5;


    private void Awake()
    {
        MakeInstance();
    }

    void Start()
    {
        Initialize();
    }

    void MakeInstance()
    {
        if (instance == null)
            instance = this;
        else if (instance != null)
            Destroy(gameObject);
    }

    void Initialize()
    {
        InitializePlataform(roadPrefab, ref lastPosOfRoadTile, roadPrefab.transform.position,
            startRoadTile, roadHolder, ref roadTilesList, ref lastOrderOfRoad, new Vector3(1.5f, 0f, 0f));

        InitializePlataform(grassPrefab, ref lastPosOfTopNearGrass, grassPrefab.transform.position,
            startGrassTile, topNearSideWalkHolder, ref topNearGrassTilesList, ref lastOrderOfTopNearGrass, new Vector3(1.2f, 0f, 0f));

        InitializePlataform(groundPrefab_3, ref lastPosOfTopFarGrass, groundPrefab_3.transform.position,
            startGround3Tile, topFarSideWalkHolder, ref topFarGrassTilesList, ref lastOrderOfTopFarGrass, new Vector3(4.8f, 0f, 0f));

        InitializePlataform(grassBottomPrefab, ref lastPosOfBottomNearGrass, new Vector3(2.0f, grassBottomPrefab.transform.position.y, 0f),
            startGrassTile, bottomNearSideWalkHolder, ref bottomNearGrassTilesList, ref lastOrderOfBottomNearGrass, new Vector3(1.2f, 0f, 0f));

        InitializeBottomForLand();
    }

    void InitializePlataform(GameObject prefab, ref Vector3 lastPos, Vector3 lastPosOfTile, int amountTile,
        GameObject holder, ref List<GameObject> listTile, ref int lastOrder, Vector3 offSet)
    {
        int orderInLayer = 0;
        lastPos = lastPosOfTile;

        for (int i = 0; i < amountTile; i++)
        {
            GameObject clone = Instantiate(prefab, lastPos, prefab.transform.rotation) as GameObject;
            clone.GetComponent<SpriteRenderer>().sortingOrder = orderInLayer;

            if (clone.tag == MyTags.TOP_NEAR_GRASS)
            {
                SetNearScene(bigGrassPrefab, ref clone, ref orderInLayer,
                    posForTopBigGrass, posForTopTree1, posForTopTree2, posForTopTree3);
            }
            else if (clone.tag == MyTags.BOTTOM_NEAR_GRASS)
            {
                SetNearScene(bigGrassBottomPrefab, ref clone, ref orderInLayer,
                    posForBottomBigGrass, posForBottomTree1, posForBottomTree2, posForBottomTree3);
            }
            else if (clone.tag == MyTags.BOTTOM_FAR_LAND_2)
            {
                if (orderInLayer == 5)
                {
                    CreateTreeOrGround(bigTreePrefab, ref clone, new Vector3(-0.57f, -1.34f, 0f));
                }
            }
            else if (clone.tag == MyTags.TOP_FAR_GRASS)
            {
                CreateGround(ref clone, ref orderInLayer);
            }

            clone.transform.SetParent(holder.transform);//tranforma o objeto em filho do pai
            listTile.Add(clone);

            orderInLayer += 1;//incrementa em +1 a ordem
            lastOrder = orderInLayer;//passa a ultima ordem da layer para a ordem

            lastPos += offSet;
        }
    }

    //cria a cena
    void CreateScene(GameObject big_Grass_Prefab, ref GameObject tileClone, int orderInLayer)
    {
        GameObject clone = Instantiate(big_Grass_Prefab, tileClone.transform.position, big_Grass_Prefab.transform.rotation) as GameObject;

        clone.GetComponent<SpriteRenderer>().sortingOrder = orderInLayer;
        clone.transform.SetParent(tileClone.transform);
        clone.transform.localPosition = new Vector3(-0.183f, 0.106f, 0f);

        CreateTreeOrGround(treePrefab_1, ref clone, new Vector3(0f, 1.52f, 0f));

        //Desligar o Tile Pai  para mostrar o tile do filho
        tileClone.GetComponent<SpriteRenderer>().enabled = false;
    }

    void CreateTreeOrGround(GameObject prefab, ref GameObject tileClone, Vector3 localPos)
    {
        GameObject clone = Instantiate(prefab, tileClone.transform.position, prefab.transform.rotation) as GameObject;

        SpriteRenderer tileCloneRenderer = tileClone.GetComponent<SpriteRenderer>();
        SpriteRenderer cloneRender = clone.GetComponent<SpriteRenderer>();

        cloneRender.sortingOrder = tileCloneRenderer.sortingOrder;
        clone.transform.SetParent(tileClone.transform);
        clone.transform.localPosition = localPos;

        if (prefab == groundPrefab_1 || prefab == groundPrefab_2 || prefab == groundPrefab_4)
        {
            tileCloneRenderer.enabled = false;
        }
    }

    void CreateGround(ref GameObject clone, ref int orderInLayer)
    {
        for (int i = 0; i < posForTopGround1.Length; i++)
        {
            if (orderInLayer == posForTopGround1[i])
            {
                CreateTreeOrGround(groundPrefab_1, ref clone, Vector3.zero);
                break;
            }
        }

        for (int i = 0; i < posForTopGround2.Length; i++)
        {
            if (orderInLayer == posForTopGround2[i])
            {
                CreateTreeOrGround(groundPrefab_2, ref clone, Vector3.zero);
                break;
            }
        }

        for (int i = 0; i < posForTopGround4.Length; i++)
        {
            if (orderInLayer == posForTopGround4[i])
            {
                CreateTreeOrGround(groundPrefab_4, ref clone, Vector3.zero);
                break;
            }
        }
    }

    void SetNearScene(GameObject bigGrassPrefab, ref GameObject clone, ref int orderInLayer,
        int[] posForBigGrass, int[] posForTree1, int[] posForTree2, int[] posForTree3)
    {
        for (int i = 0; i < posForBigGrass.Length; i++)
        {
            if (orderInLayer == posForBigGrass[i])
            {
                CreateScene(bigGrassPrefab, ref clone, orderInLayer);
                break;
            }
        }

        for (int i = 0; i < posForTree1.Length; i++)//instancia a arvore 1
        {
            if (orderInLayer == posForTree1[i])
            {
                CreateTreeOrGround(treePrefab_1, ref clone, new Vector3(0f, 1.15f, 0f));
                break;
            }
        }

        for (int i = 0; i < posForTree2.Length; i++)//instancia a arvore 2
        {
            if (orderInLayer == posForTree2[i])
            {
                CreateTreeOrGround(treePrefab_2, ref clone, new Vector3(0f, 1.15f, 0f));
                break;
            }
        }

        for (int i = 0; i < posForTree3.Length; i++)//instancia a arvore 3
        {
            if (orderInLayer == posForTree3[i])
            {
                CreateTreeOrGround(treePrefab_3, ref clone, new Vector3(0f, 1.15f, 0f));
                break;
            }
        }
    }

    void InitializeBottomForLand()
    {
        InitializePlataform(landPrefab_1, ref lastPosOfBottomFarLandF1, landPrefab_1.transform.position,
            startLandTile, bottomFarSideWalkHolder, ref bottomFarLandF1TilesList, ref lastOrderOfBottomFarLandF1, new Vector3(1.6f, 0f, 0f));

        InitializePlataform(landPrefab_2, ref lastPosOfBottomFarLandF2, landPrefab_2.transform.position,
            startLandTile - 3, bottomFarSideWalkHolder, ref bottomFarLandF2TilesList, ref lastOrderOfBottomFarLandF2, new Vector3(1.6f, 0f, 0f));

        InitializePlataform(landPrefab_3, ref lastPosOfBottomFarLandF3, landPrefab_3.transform.position,
            startLandTile - 4, bottomFarSideWalkHolder, ref bottomFarLandF3TilesList, ref lastOrderOfBottomFarLandF3, new Vector3(1.6f, 0f, 0f));

        InitializePlataform(landPrefab_4, ref lastPosOfBottomFarLandF4, landPrefab_4.transform.position,
            startLandTile - 7, bottomFarSideWalkHolder, ref bottomFarLandF4TilesList, ref lastOrderOfBottomFarLandF4, new Vector3(1.6f, 0f, 0f));

        InitializePlataform(landPrefab_5, ref lastPosOfBottomFarLandF5, landPrefab_5.transform.position,
            startLandTile - 10, bottomFarSideWalkHolder, ref bottomFarLandF5TilesList, ref lastOrderOfBottomFarLandF5, new Vector3(1.6f, 0f, 0f));
    }
}
