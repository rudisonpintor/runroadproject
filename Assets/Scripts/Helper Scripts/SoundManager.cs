﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager instance;

    public AudioSource
        moveAudioSource,
        jumpAudioSource,
        powerUpAudioSource,
        backgroundAudioSource;

    public AudioClip
        powerUpClip,
        dieClip,
        coinClip,
        gameOverClip;

    private void Awake()
    {
        MakeInstance();
    }
    void Start()
    {
        if (GameManager.instance.playSound)
        {
            backgroundAudioSource.Play();
        }
        else
            backgroundAudioSource.Stop();
    }

    void MakeInstance()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            Destroy(gameObject);
        }
    }

    public void PlayMoveLineSound()
    {
        moveAudioSource.Play();
    }

    public void PlayJumpSound()
    {
        jumpAudioSource.Play();
    }

    public void PlayDeadSound()
    {
        powerUpAudioSource.clip = dieClip;
        powerUpAudioSource.Play();
    }

    public void PlayPowerUpSound()
    {
        powerUpAudioSource.clip = powerUpClip;
        powerUpAudioSource.Play();
    }

    public void PlayCoinSound()
    {
        powerUpAudioSource.clip = coinClip;
        powerUpAudioSource.Play();
    }

    public void PlayGameOverClip()
    {
        backgroundAudioSource.Stop();
        backgroundAudioSource.clip = gameOverClip;
        backgroundAudioSource.loop = false;
        backgroundAudioSource.Play();
    }
}
