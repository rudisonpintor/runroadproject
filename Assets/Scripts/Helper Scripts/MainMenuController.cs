﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour
{
    public GameObject heroMenu;
    public Text txtStarScore;

    public Image imgMusic;
    public Sprite musicON, musicOFF;

    public GameObject infoMenu;

    public void PlayGame()
    {
        SceneManager.LoadScene("GamePlay");
    }

    public void HeroMenu()
    {
        heroMenu.SetActive(true);
        txtStarScore.text = "" + GameManager.instance.starScore;
    }

    public void HomeButton()
    {
        heroMenu.SetActive(false);
    }

    public void MusicButton()
    {
        if (GameManager.instance.playSound)
        {
            imgMusic.sprite = musicOFF;
            GameManager.instance.playSound = false;
        }
        else
        {
            imgMusic.sprite = musicON;
            GameManager.instance.playSound = true;
        }
    }

    public void BtnInfo()
    {
        infoMenu.SetActive(true);
    }

    public void BtnHomeInfo()
    {
        infoMenu.SetActive(false);
    }
}
