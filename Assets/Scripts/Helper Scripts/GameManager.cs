﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    private GameData gameData;

    [HideInInspector]
    public int starScore, scoreCount, selectedIndex;

    [HideInInspector]
    public bool[] heroes;

    [HideInInspector]
    public bool playSound = true;

    private string Path = "GameData.dat";

    private void Awake()
    {
        MakeSingleton();

        InitializeGameData();
    }

    void MakeSingleton()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        //print(Application.persistentDataPath + Path);
    }

    void InitializeGameData()
    {
        LoadGameData();

        if (gameData == null)
        {
            starScore = 5000;
            scoreCount = 0;
            selectedIndex = 0;

            heroes = new bool[9];
            heroes[0] = true;

            for (int i = 1; i < heroes.Length; i++)//desailita todos os outros
            {
                heroes[i] = false;
            }

            gameData = new GameData();
            gameData.StarScore = starScore;
            gameData.ScoreCount = scoreCount;
            gameData.Heroes = heroes;
            gameData.SelectedIndex = selectedIndex;

            SaveGameData();
        }
    }

    public void SaveGameData()
    {
        FileStream file = null;

        try
        {
            BinaryFormatter bg = new BinaryFormatter();

            file = File.Create(Application.persistentDataPath + Path);

            if (gameData != null)
            {
                gameData.Heroes = heroes;
                gameData.StarScore = starScore;
                gameData.ScoreCount = scoreCount;
                gameData.SelectedIndex = selectedIndex;

                bg.Serialize(file, gameData);
            }
        }
        catch (Exception ex)
        {
        }
        finally
        {
            if (file != null)
            {
                file.Close();
            }
        }
    }

    void LoadGameData()
    {
        FileStream file = null;
        try
        {
            BinaryFormatter bf = new BinaryFormatter();

            file = File.Open(Application.persistentDataPath + Path, FileMode.Open);

            gameData = (GameData)bf.Deserialize(file);

            if (gameData != null)
            {
                starScore = gameData.StarScore;
                scoreCount = gameData.ScoreCount;
                heroes = gameData.Heroes;
                selectedIndex = gameData.SelectedIndex;
            }
        }
        catch (Exception ex)
        {
        }
        finally
        {
            if (file != null)
                file.Close();
        }
    }
}
