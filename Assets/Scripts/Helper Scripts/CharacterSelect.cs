﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSelect : MonoBehaviour
{
    public GameObject[] avaliableHeroes;

    private int currentIndex;

    public Text selectedText;
    public GameObject starIcon;
    public Image btnSelectImg;
    public Sprite buttonGreen, buttonBlue;

    private bool[] heroes;

    public Text txtStarCore;

    void Start()
    {
        InitializeCharacters();
    }

    void InitializeCharacters()
    {
        currentIndex = GameManager.instance.selectedIndex;

        for (int i = 0; i < avaliableHeroes.Length; i++)
        {
            avaliableHeroes[i].SetActive(false);
        }
        avaliableHeroes[currentIndex].SetActive(true);

        heroes = GameManager.instance.heroes;
    }

    public void NextHero()
    {
        avaliableHeroes[currentIndex].SetActive(false);

        if (currentIndex + 1 == avaliableHeroes.Length)
        {
            currentIndex = 0;
        }
        else
        {
            currentIndex++;
        }

        avaliableHeroes[currentIndex].SetActive(true);
        CheckIfCharacterIsUnlocked();
    }

    public void PreviousHero()
    {
        avaliableHeroes[currentIndex].SetActive(false);

        if (currentIndex - 1 == -1)
        {
            currentIndex = avaliableHeroes.Length - 1;
        }
        else
        {
            currentIndex--;
        }
        avaliableHeroes[currentIndex].SetActive(true);
        CheckIfCharacterIsUnlocked();
    }

    void CheckIfCharacterIsUnlocked()
    {
        if (heroes[currentIndex])
        {
            starIcon.SetActive(false);

            if (currentIndex == GameManager.instance.selectedIndex)
            {
                btnSelectImg.sprite = buttonGreen;
                selectedText.text = "SELECTED";
            }
            else
            {
                btnSelectImg.sprite = buttonBlue;
                selectedText.text = "SELECT?";
            }
        }
        else
        {
            btnSelectImg.sprite = buttonBlue;
            starIcon.SetActive(true);
            selectedText.text = "1000";
        }
    }

    public void SelectHero()
    {
        if (!heroes[currentIndex])
        {
            if (currentIndex != GameManager.instance.selectedIndex)
            {
                if (GameManager.instance.starScore >= 1000)
                {
                    GameManager.instance.starScore -= 1000;

                    btnSelectImg.sprite = buttonGreen;
                    selectedText.text = "SELECTED";
                    starIcon.SetActive(false);
                    heroes[currentIndex] = true;

                    txtStarCore.text = GameManager.instance.starScore.ToString();

                    GameManager.instance.selectedIndex = currentIndex;
                    GameManager.instance.heroes = heroes;

                    GameManager.instance.SaveGameData();
                }
                else
                {
                    print("SEM DINHEIRO");
                }
            }
        }
        else//se o heroi for desbloqueado, apenas selecione
        {
            btnSelectImg.sprite = buttonGreen;
            selectedText.text = "SELECTED";

            GameManager.instance.selectedIndex = currentIndex;
            GameManager.instance.SaveGameData();
        }
    }
}
