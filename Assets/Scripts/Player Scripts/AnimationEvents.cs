﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEvents : MonoBehaviour
{
    private Animator anim;

    private string walkAnimation = "Walk";

    private void Awake()
    {
        anim = GetComponent<Animator>();
    }

    void Start()
    {
        
    }

    void PlayerWalkAnimation()
    {
        anim.Play(walkAnimation);

        if (PlayerController.instance.isPlayerJumped)
        {
            PlayerController.instance.isPlayerJumped = false;
        }
    }

    void AnimationEnded()
    {
        gameObject.SetActive(false);
    }

    void PausePanelClose()
    {
        Time.timeScale = 1;
        gameObject.SetActive(false);
    }
}
