﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController instance;

    private Animator anim;

    private string jumpAnimation = "Jump";
    private string changeLineAnimation = "ChangeLine";

    public GameObject player, shadow;

    public Vector3 firstPosOfPlayer, secondPosOfPlayer;

    [HideInInspector]
    public bool isPlayerDied;

    [HideInInspector]
    public bool isPlayerJumped;

    public GameObject explosion;

    private SpriteRenderer spriteRenderer;
    public Sprite TRexSprite, playerSprite;
    private bool isTRexTrigger;
    private GameObject[] starEffect;

    private void Awake()
    {
        MakeInstance();

        anim = player.GetComponent<Animator>();
        spriteRenderer = player.GetComponent<SpriteRenderer>();
        starEffect = GameObject.FindGameObjectsWithTag(MyTags.STAR_EFFECT);
    }

    void Start()
    {
        string path = "Sprites/Player/hero" + GameManager.instance.selectedIndex + "_big";
        playerSprite = Resources.Load<Sprite>(path);
        spriteRenderer.sprite = playerSprite;
    }

    void Update()
    {
        HandleChangeLine();
        HandleJump();
    }

    void MakeInstance()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            Destroy(gameObject);
        }
    }

    void HandleChangeLine()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
        {
            anim.Play(changeLineAnimation);
            transform.localPosition = secondPosOfPlayer;
            //Som do Player
            SoundManager.instance.PlayMoveLineSound();
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
        {
            anim.Play(changeLineAnimation);
            transform.localPosition = firstPosOfPlayer;
            //Som do Player
            SoundManager.instance.PlayMoveLineSound();
        }
    }

    void HandleJump()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (!isPlayerJumped)
            {
                anim.Play(jumpAnimation);
                isPlayerJumped = true;
                SoundManager.instance.PlayJumpSound();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == MyTags.OBSTACLE)
        {
            if (!isTRexTrigger)
            {
                DieWithObstacle(col);
            }
            else
            {
                DestroyObstacle(col);
            }
        }

        if (col.tag == MyTags.T_REX)
        {
            isTRexTrigger = true;
            spriteRenderer.sprite = TRexSprite;
            col.gameObject.SetActive(false);

            SoundManager.instance.PlayPowerUpSound();

            StartCoroutine(TRexDurationRoutine());
        }

        if (col.tag == MyTags.STAR)
        {
            for (int i = 0; i < starEffect.Length; i++)
            {
                if (!starEffect[i].activeInHierarchy)
                {
                    starEffect[i].transform.position = col.transform.position;
                    starEffect[i].SetActive(true);
                    break;
                }
            }
            col.gameObject.SetActive(false);
            SoundManager.instance.PlayCoinSound();
            GamePlayController.instance.UpdateStarScore();
        }
    }

    void Die()
    {
        isPlayerDied = true;
        player.SetActive(false);
        shadow.SetActive(false);

        GamePlayController.instance.moveSpeed = 0;
        GamePlayController.instance.GameOver();
        SoundManager.instance.PlayGameOverClip();
    }

    void DieWithObstacle(Collider2D col)
    {
        Die();
        explosion.transform.position = col.transform.position;
        explosion.SetActive(true);

        col.gameObject.SetActive(false);

        //Som Manager Player Dead
        SoundManager.instance.PlayDeadSound();
    }

    IEnumerator TRexDurationRoutine()
    {
        yield return new WaitForSeconds(7);

        if (isTRexTrigger)
        {
            isTRexTrigger = false;
            spriteRenderer.sprite = playerSprite;
        }
    }

    void DestroyObstacle(Collider2D target)
    {
        explosion.transform.position = target.transform.position;
        explosion.SetActive(false);
        explosion.SetActive(true);

        target.gameObject.SetActive(false);
        SoundManager.instance.PlayDeadSound();
    }
}
