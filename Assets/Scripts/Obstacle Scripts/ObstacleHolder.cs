﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleHolder : MonoBehaviour
{
    public GameObject[] childs;

    public float limitAxisX;

    public Vector3 firstPos, secondPos;

    void Start()
    {
        
    }

    void Update()
    {
        transform.position += new Vector3(-GamePlayController.instance.moveSpeed * Time.deltaTime, 0f, 0f);

        if (transform.localPosition.x < limitAxisX)
        {
            GamePlayController.instance.isObstaclesActive = false;
            gameObject.SetActive(false);
        }
    }

    private void OnEnable()//É Chamado em segundo no carregamento do game, porem a diferenca que toda vez que um objeto
        //ativa ou desativa ele eh chamado novamente.
    {
        for (int i = 0; i < childs.Length; i++)
        {
            childs[i].SetActive(true);
        }

        if (Random.value <= 0.5f)
        {
            transform.localPosition = firstPos;
        }
        else
            transform.localPosition = secondPos;
    }
}
